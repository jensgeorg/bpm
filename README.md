# bpm - Blood pressure monitor

BPM is a tool do download and visualize the blood pressure monitor data from the microlife professional devices.

It supports:

* Backing up data from the device because the device has limited memory
* Calculate various statistics from the data
* Provide nice visualization of the blood pressure data
  * progress over selectable time spans
  * X/Y graph which clusters the pressure depending on the "good/bad" status of the systolic and diastolic pressures
* Create print-outs to give to your doctor

